package com.whodrive.renan.whodrive;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final int CAMERA_REQUEST_CODE = 1;
    private List<GoingOut> goingOutList;
    private Bitmap bitmap;
    private String drivers_name;
    private String member_meeting;
    private String number_of_cars;

    GoingOutAdapter goingOutAdapter;
    private ImageButton takePhoto;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        long date = System.currentTimeMillis();

        SimpleDateFormat sdf = new SimpleDateFormat(" dd.MM.yyyy");

        final String dateString = sdf.format(date);
        try {
            loadData();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("ERRORRR","asdasdasd");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        goingOutAdapter = new GoingOutAdapter(goingOutList);

        goingOutAdapter.setListener(new GoingOutAdapter.MyGoingOutListenerIO() {

            @Override
            public void onItemClicked(final int position, View view) {//only update the item in recycleView
                Intent intent = new Intent(MainActivity.this, OpenDetails.class);
                intent.putExtra("drivers_name",goingOutList.get(position).getmDriversName());
                intent.putExtra("member_meeting",goingOutList.get(position).getmMembersMeeting());
                intent.putExtra("number_of_cars",goingOutList.get(position).getmNumberOfCars());
                intent.putExtra("date",goingOutList.get(position).getmDate());
                intent.putExtra("index",position);
                intent.putExtra("goingOutObject", goingOutList.get(position)); 

                startActivity(intent);
            }
            @Override
            public void onLongItemClicked(int position, View view) { }
        });


        ItemTouchHelper.Callback callback = new ItemTouchHelper.Callback() {
            @Override
            public boolean onMove( RecyclerView recyclerView,  RecyclerView.ViewHolder dragged,  RecyclerView.ViewHolder target) {
                Collections.swap(goingOutList, dragged.getAdapterPosition(), target.getAdapterPosition());
                // and notify the adapter that its dataset has changed
                goingOutAdapter.notifyItemMoved(dragged.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setCancelable(true);
                builder.setTitle("Confirm Delete");
                builder.setMessage("Are you sure you want to Delete");

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        goingOutAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                    }
                });

                builder.setPositiveButton("O.K.", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goingOutList.remove(viewHolder.getAdapterPosition());
                        goingOutAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                        saveData();
                        Toast.makeText(MainActivity.this, "Removed", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.show();
            }
            //defines the enabled move directions in each state (idle, swiping, dragging).
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(goingOutAdapter);
        goingOutAdapter.notifyDataSetChanged();

        FloatingActionButton addBtn = findViewById(R.id.floating_add_btn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog thisDialog = new Dialog(MainActivity.this);
                thisDialog.setContentView(R.layout.layout_dialog);
                thisDialog.setTitle("Add New Friends Meeting");
                thisDialog.show();

                Button cancel_Btn = thisDialog.findViewById(R.id.cancel_btn_d);
                cancel_Btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        thisDialog.cancel();
                    }
                });
                takePhoto = thisDialog.findViewById(R.id.image_btn_d);
                takePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, CAMERA_REQUEST_CODE);
                    }
                });

                Button ok_Btn = thisDialog.findViewById(R.id.ok_btn_d);
                ok_Btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText drivers_name_et = thisDialog.findViewById(R.id.Drivers_Name_d);
                        EditText member_meeting_et = thisDialog.findViewById(R.id.Member_Meeting_d);
                        EditText number_of_cars_et = thisDialog.findViewById(R.id.Number_Of_Cars_d);

                        drivers_name = drivers_name_et.getText().toString();
                        member_meeting = member_meeting_et.getText().toString();
                        number_of_cars = number_of_cars_et.getText().toString();

                        if ((drivers_name.equals("")) || (member_meeting.equals("")) || (number_of_cars.equals(""))) {
                            Toast.makeText(MainActivity.this, "Fill all the missing places", Toast.LENGTH_LONG).show();
                        } else {
                            goingOutList.add(0,new GoingOut(drivers_name, member_meeting, number_of_cars,dateString, goingOutList.get(0).getmPicture()));
                            goingOutAdapter.notifyItemInserted(0);
                            saveData();
                            thisDialog.dismiss();
                        }
                    }
                });
            }
        });
    }

    private void saveData() {
        try {
            FileOutputStream fos = openFileOutput("meeting", MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(goingOutList);
            oos.close();
        } catch (FileNotFoundException e) {
                e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void loadData() throws IOException, ClassNotFoundException {
        if (goingOutList == null)
        {
            goingOutList = new ArrayList<>();
            goingOutList.add(0,new GoingOut("Eliad", "Eliad, Itay", "1","02.04.2019",BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.meeting2)));
            goingOutList.add(0,new GoingOut("Ronel", "Ronel, Eden", "1","24.03.2019",BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.meeting3)));
            goingOutList.add(0,new GoingOut("Gal", "Gal, Michal", "1","29.03.2019",BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.meeting)));

        }
        File file = new File(getFilesDir(),"meeting");
        file.createNewFile();
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        goingOutList = (List<GoingOut>)ois.readObject();
        ois.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (data != null && data.getExtras() != null) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    this.bitmap = bitmap;
                    takePhoto.setImageBitmap(bitmap);
                    goingOutList.get(0).setmPicture(this.bitmap);
                }
                break;

        }
    }
}
