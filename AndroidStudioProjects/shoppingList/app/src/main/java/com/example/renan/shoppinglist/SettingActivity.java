package com.example.renan.shoppinglist;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingActivity extends Activity {

    public static final int UPDATE_PHONE_NUMBER=3;
    public static final int GALLERY_REQUEST = 4;
    private String phoneNumber;
    private TextView phoneNumberTv;
    private ImageButton cameraBtn;
    private String selectedImagePath;
    private ImageView photoIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        phoneNumber = getIntent().getStringExtra("phone_number");
        phoneNumberTv = findViewById(R.id.phone_number);
        phoneNumberTv.setText(phoneNumber);
        ImageButton addAlarmBtn = findViewById(R.id.add_new_alarm);
        addAlarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, AddAlarmActivity.class);
                startActivity(intent);
            }
        });

        ImageButton contactBtn = findViewById(R.id.contact_btn);
        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, AddContactActivity.class);
                startActivity(intent);
            }
        });

        photoIv = findViewById(R.id.image_from_gallery_Iv);

        cameraBtn = findViewById(R.id.camera_btn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, ""),GALLERY_REQUEST );

            }
        });

        ImageButton editPhonNumberBtn = findViewById(R.id.edit_phoneNumber_btn);
        editPhonNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this,EditPhoneNumberActivity.class);
                intent.putExtra("phone_number", phoneNumber);
                startActivityForResult(intent, UPDATE_PHONE_NUMBER);
            }
        });
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (UPDATE_PHONE_NUMBER) : {
                if (resultCode == Activity.RESULT_OK) {
                    phoneNumber = data.getStringExtra("updated_phone_number");
                    phoneNumberTv.setText(phoneNumber);
                    Intent intent = new Intent(SettingActivity.this,MainActivity.class);
                    intent.putExtra("updated_phone_number",phoneNumber);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
                break;
            }
            case GALLERY_REQUEST :
                if (resultCode == RESULT_OK) {

                    Uri selectedImageUri = data.getData();
                    selectedImagePath = getPath(selectedImageUri);
                    System.out.println("Image Path : " + selectedImagePath);
                    photoIv.setImageURI(selectedImageUri);

                }
                break;
        }
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
