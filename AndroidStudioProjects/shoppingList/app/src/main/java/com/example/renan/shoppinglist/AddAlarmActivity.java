package com.example.renan.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddAlarmActivity extends Activity {

    private EditText hourEt;
    private EditText minuteEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alarm);

        hourEt = findViewById(R.id.hour_Et);
        minuteEt = findViewById(R.id.min_Et);

        Button newAlarmBtn = findViewById(R.id.add_alarm);
        newAlarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = Integer.parseInt(hourEt.getText().toString());
                int minute = Integer.parseInt(minuteEt.getText().toString());

                Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
                intent.putExtra(AlarmClock.EXTRA_HOUR, hour);
                intent.putExtra(AlarmClock.EXTRA_MINUTES, minute);
                if (hour <=24 && minute<=60)
                {
                    startActivity(intent);
                    finish();
                }
                else
                    {
                        Toast.makeText(AddAlarmActivity.this, R.string.unable_set_hour, Toast.LENGTH_SHORT).show();
                    }
            }
        });
    }
}
